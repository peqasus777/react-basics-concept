import './slider.css'

export const Slider = () => {
    return (
      <div>
      <label className="switch" htmlFor="checkbox">
      <input type="checkbox" id="checkbox" />
      <div className="slider round"></div>
      </label>
      </div>
    )
  }