import React from "react"; 
import './App.css'
import { Slider } from "./components/Slider";


const Title = () => { 
  return React.createElement("h1", { style: {color: '#999', fontSize: '19px' } }, "Solar system planets"); 
}; 


function App() {

  return (
    <>
      <Title/>
      <Slider/>
      <ul className="planets-list">
      <li> Mercury</li>
      <li>Venus</li>
      <li>Earth</li>
      <li>Mars</li>
      <li>Jupiter</li>
      <li>Saturn</li>
      <li>Uranus</li>
      <li>Neptune</li>
      </ul>

    </>
  )
};

export default App;
